from __future__ import unicode_literals

from django.db import models


# Create your models here.

class Dress(models.Model):
    name = models.CharField(max_length=500,verbose_name="Option Name")
    price = models.DecimalField(max_digits=10, decimal_places=2,verbose_name="Price")


class Option(models.Model):
    name = models.CharField(max_length=500,verbose_name="Option Name")
    type=models.IntegerField(verbose_name="Select Type",choices=[(0,"radio"),(1,"select")])
    dress = models.ForeignKey(Dress)


class Choice(models.Model):
    name = models.CharField(max_length=500, verbose_name="Choice Name")
    extra_charge = models.DecimalField(default=0, max_digits=10, decimal_places=2,verbose_name="Extra Charge")
    option = models.ForeignKey(Option)
