# coding:utf-8
# author:Nick
from django.forms.utils import ErrorList

__all__ = ['DressFrom']
from models import Dress, Option, Choice
import django.forms as forms


class DressFrom(forms.ModelForm):
    class Meta:
        model = Dress
        fields = "__all__"


class OptionForm(forms.ModelForm):
    class Meta:
        model = Option
        fields = ["name", "type"]


class ChoiceForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ["name", "extra_charge"]


class DressFormForConsumer(forms.Form):
    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, field_order=None, use_required_attribute=None, options=[]):

        """
        Options are records  from Option Model belonging to current dress containing some choices,
         pass options parameter each time when you initial this form class, 
         so you can control multiple options,
         generate  radio or multiple-select automatically.
        initial the form.
        :param data: 
        :param files: 
        :param auto_id: 
        :param prefix: 
        :param initial: 
        :param error_class: 
        :param label_suffix: 
        :param empty_permitted: 
        :param field_order: 
        :param use_required_attribute: 
        :param options: [Option]
        """
        super(DressFormForConsumer, self).__init__(data, files, auto_id, prefix, initial, error_class, label_suffix,
                                                   empty_permitted, field_order, use_required_attribute)
        for op in options:
            choices = Choice.objects.filter(option=op)
            if op.type == 0:
                self.fields[op.name] = forms.ChoiceField(choices=choices,widget=forms.RadioSelect())
            else:
                self.fields[op.name] = forms.MultipleChoiceField(choices=[(c.id, c.name) for c in choices], )
