from django.shortcuts import render
from forms import DressFrom, ChoiceForm, OptionForm, DressFormForConsumer
from models import Dress, Option


# Create your views here.



#     Use this form ,You can control whether one option is radio or multiple select
#     see DressFormForConsumer.__init__ method
def index(request):
    form = DressFormForConsumer()


def list_dresses(request):
    """
    List All Dresses
    :param request: 
    :return: 
    """
    dresses = Dress.objects.all()
    for d in dresses:
        d.options = Option.objects.filter(dress=d)
    return render(request, "dresses.html", locals())


def handler(request, form_class):
    """
    
    :param request: 
    :param form_class: 
    :type form_class:type
    :return: 
    """

    form = form_class()
    if request.method == "POST":
        form = form_class(request.POST)
        form.save()
        return render(request, "success.html", locals())

    return render(request, "base.html", locals())


def add_dress(request):
    return handler(request, DressFrom)


def add_choice(request):
    form = ChoiceForm()
    option_id = request.GET.get("oid", False)
    if not option_id:
        return render(request, "error.html", {"error": "Option Id can not be empty"})
    if request.method == "POST":
        form = ChoiceForm(request.POST)
        form.instance.option_id = int(option_id)
        form.save()
        return render(request, "success.html")

    return render(request, "base.html", locals())


def add_option(request):
    form = OptionForm()
    dress_id = request.GET.get("did", False)
    if not dress_id:
        return render(request, "error.html", {"error": "Dress Id can not be empty"})
    if request.method == "POST":
        form = OptionForm(request.POST)
        form.instance.dress_id = int(dress_id)
        form.save()
        return render(request, "success.html")
    return render(request, "base.html", locals())
